package nl.bioinf.wis;

public class TestSequences {
    public static final String SHORT_LEGAL_SEQUENCE_UPPER = "AGCTGGCATTA"; //yes this is protein!
    public static final String SHORT_LEGAL_SEQUENCE_LOWER = "mlqrwivilpvsde";
    public static final String SHORT_ILLEGAL_SEQUENCE_LOWER = "mxlqrwiuilpvsde";
    public static final String LONG_LEGAL_SEQUENCE_UPPER = "MSFKFGQHLIKPSVVFLKTELSFALVNRKPVVPGHVLMCPLRPVERFRDLRPDEVADLFQVTQRVGTVVEKHFQGTSITFSMQDGPEAGQTVKHVHVHILPRKSGDFRRNDNIYDELQKHDREEEDSPAFWRSEEEMAAEAEVLRAYFQA";
}
