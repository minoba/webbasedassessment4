package nl.bioinf.wis;

import java.util.*;

public class AminoAcid {
    public static final AminoAcid UNKNOWN = new AminoAcid('X');

    private static final Map<Character, AminoAcid> ALL_AMINO_ACIDS = new HashMap<>();

    private char singleCharacter;
    private String threeLetterCode;
    private String fullName;
    private double molecularWeight;
    private double hydrophobicity;
    private boolean isHydrophobic;
    private Size size;
    private int charge;
    private boolean isPolar;

    /**
     * Constructs with a single-letter representation
     *
     * @param aminoAcid the amino acid code
     */
    private AminoAcid(char aminoAcid) {
        this.singleCharacter = aminoAcid;
        initialise();
    }

    /**
     * Returns all known amino acids
     * @return all
     */
    public static List<AminoAcid> getAll() {
        List<AminoAcid> all = new ArrayList<>(ALL_AMINO_ACIDS.values());
        return all;
    }

    /**
     * Factory method for AminoAcid instances.
     * @param aminoAcidCode
     * @return Optional<AminoAcid>
     */
    public static AminoAcid of(char aminoAcidCode) {
        if(ALL_AMINO_ACIDS.containsKey(aminoAcidCode)) {
            return ALL_AMINO_ACIDS.get(aminoAcidCode);
        } else {
            return UNKNOWN;
        }
    }

    public char getSingleCharacter() {
        return singleCharacter;
    }

    public String getThreeLetterCode() {
        return threeLetterCode;
    }

    public String getFullName() {
        return fullName;
    }

    public double getMolecularWeight() {
        return molecularWeight;
    }

    public boolean isHydrophobic() {
        return isHydrophobic;
    }

    public boolean isPolar() {
        return isPolar;
    }

    public int getCharge() {
        return charge;
    }

    public double getHydrophobicity() {
        return hydrophobicity;
    }

    public Size getSize() {
        return size;
    }

    /**
     * Abstracts away the size of amino acids
     */
    public enum Size {
        SIZE_SMALL(0), SIZE_MEDIUM(1), SIZE_LARGE(2), SIZE_VERY_LARGE(3);
        private int value;

        public int getValue() {
            return value;
        }

        Size(int value) {
            this.value = value;
        }
    }

    private void setIsHydrophobic(boolean isHydrophobic) {
        this.isHydrophobic = isHydrophobic;
    }

    private void setIsPolar(boolean isPolar) {
        this.isPolar = isPolar;
    }

    private void setCharge(int charge) {
        this.charge = charge;
    }

    private void setHydrophobicity(double hydrophobicity) {
        this.hydrophobicity = hydrophobicity;
    }

    static {
        ALL_AMINO_ACIDS.put('A', new AminoAcid('A'));
        ALL_AMINO_ACIDS.put('C', new AminoAcid('C'));
        ALL_AMINO_ACIDS.put('D', new AminoAcid('D'));
        ALL_AMINO_ACIDS.put('E', new AminoAcid('E'));
        ALL_AMINO_ACIDS.put('F', new AminoAcid('F'));
        ALL_AMINO_ACIDS.put('G', new AminoAcid('G'));
        ALL_AMINO_ACIDS.put('H', new AminoAcid('H'));
        ALL_AMINO_ACIDS.put('I', new AminoAcid('I'));
        ALL_AMINO_ACIDS.put('K', new AminoAcid('K'));
        ALL_AMINO_ACIDS.put('L', new AminoAcid('L'));
        ALL_AMINO_ACIDS.put('M', new AminoAcid('M'));
        ALL_AMINO_ACIDS.put('N', new AminoAcid('N'));
        ALL_AMINO_ACIDS.put('P', new AminoAcid('P'));
        ALL_AMINO_ACIDS.put('Q', new AminoAcid('Q'));
        ALL_AMINO_ACIDS.put('R', new AminoAcid('R'));
        ALL_AMINO_ACIDS.put('S', new AminoAcid('S'));
        ALL_AMINO_ACIDS.put('T', new AminoAcid('T'));
        ALL_AMINO_ACIDS.put('V', new AminoAcid('V'));
        ALL_AMINO_ACIDS.put('W', new AminoAcid('W'));
        ALL_AMINO_ACIDS.put('Y', new AminoAcid('Y'));
    }

    /*create all*/
    private void initialise() {
        switch (this.singleCharacter) {
            //ALIPHATIC AMINO ACIDS
            case 'G':
                this.fullName = "Glycine";
                this.threeLetterCode = "Gly";
                this.molecularWeight = 57;
                this.size = Size.SIZE_SMALL;
                setCharge(0);
                setHydrophobicity(0.67);
                setIsHydrophobic(false);
                setIsPolar(false);
                break;
            case 'A':
                this.fullName = "Alanine";
                this.threeLetterCode = "Ala";
                this.molecularWeight = 71;
                this.size = Size.SIZE_SMALL;
                setCharge(0);
                setHydrophobicity(1.0);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;
            case 'V':
                this.fullName = "Valine";
                this.threeLetterCode = "Val";
                this.molecularWeight = 99;
                this.size = Size.SIZE_MEDIUM;
                setCharge(0);
                setHydrophobicity(2.30);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;
            case 'L':
                this.fullName = "Leucine";
                this.threeLetterCode = "Leu";
                this.molecularWeight = 113;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(2.2);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;
            case 'I':
                this.fullName = "Isoleucine";
                this.threeLetterCode = "Ile";
                this.molecularWeight = 113;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(3.1);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;
            case 'P':
                this.fullName = "Proline";
                this.threeLetterCode = "Pro";
                this.molecularWeight = 97;
                this.size = Size.SIZE_MEDIUM;
                setCharge(0);
                setHydrophobicity(-0.29);
                setIsHydrophobic(false);
                setIsPolar(false);
                break;

            //AROMATIC AMINO ACIDS
            case 'F':
                this.fullName = "Phenylalanine";
                this.threeLetterCode = "Phe";
                this.molecularWeight = 147;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(2.5);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;
            case 'Y':
                this.fullName = "Tyrosine";
                this.threeLetterCode = "Tyr";
                this.molecularWeight = 163;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(0.08);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'W':
                this.fullName = "Tryptophan";
                this.threeLetterCode = "Trp";
                this.molecularWeight = 186;
                this.size = Size.SIZE_VERY_LARGE;
                setCharge(0);
                setHydrophobicity(1.5);
                setIsHydrophobic(true);
                setIsPolar(true);
                break;

            //POLAR BUT UNCHARGED
            case 'S':
                this.fullName = "Serine";
                this.threeLetterCode = "Ser";
                this.molecularWeight = 87;
                this.size = Size.SIZE_SMALL;
                setCharge(0);
                setHydrophobicity(-1.1);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'T':
                this.fullName = "Threonine";
                this.threeLetterCode = "Thr";
                this.molecularWeight = 101;
                this.size = Size.SIZE_MEDIUM;
                setCharge(0);
                setHydrophobicity(-0.75);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'N':
                this.fullName = "Asparagine";
                this.threeLetterCode = "Asn";
                this.molecularWeight = 114;
                this.size = Size.SIZE_MEDIUM;
                setCharge(0);
                setHydrophobicity(-2.70);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'Q':
                this.fullName = "Glutamine";
                this.threeLetterCode = "Gln";
                this.molecularWeight = 128;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(-2.90);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;

            //SULPHUR CONTAINING
            case 'C':
                this.fullName = "Cysteine";
                this.threeLetterCode = "Cys";
                this.molecularWeight = 103;
                this.size = Size.SIZE_SMALL;
                setCharge(0);
                setHydrophobicity(0.17);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'M':
                this.fullName = "Methionine";
                this.threeLetterCode = "Met";
                this.molecularWeight = 131;
                this.size = Size.SIZE_LARGE;
                setCharge(0);
                setHydrophobicity(1.1);
                setIsHydrophobic(true);
                setIsPolar(false);
                break;

            //CHARGED
            case 'D':
                this.fullName = "Aspartate";
                this.threeLetterCode = "Asp";
                this.molecularWeight = 115;
                this.size = Size.SIZE_MEDIUM;
                setCharge(-1);
                setHydrophobicity(-3.0);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'E':
                this.fullName = "Glutamate";
                this.threeLetterCode = "Glu";
                this.molecularWeight = 129;
                this.size = Size.SIZE_LARGE;
                setCharge(-1);
                setHydrophobicity(-2.6);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'H':
                this.fullName = "Histidine";
                this.threeLetterCode = "His";
                this.molecularWeight = 137;
                this.size = Size.SIZE_VERY_LARGE;
                setCharge(0);
                setHydrophobicity(-1.7);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'K':
                this.fullName = "Lysine";
                this.threeLetterCode = "Lys";
                this.molecularWeight = 128;
                this.size = Size.SIZE_LARGE;
                setCharge(1);
                setHydrophobicity(-4.6);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;
            case 'R':
                this.fullName = "Arginine";
                this.threeLetterCode = "Arg";
                this.molecularWeight = 159;
                this.size = Size.SIZE_VERY_LARGE;
                setCharge(1);
                setHydrophobicity(-7.5);
                setIsHydrophobic(false);
                setIsPolar(true);
                break;

            case 'X':
                this.fullName = "Unknown";
                this.threeLetterCode = "???";
                //average of all amino acids
                this.molecularWeight = 118.9;
                break;

        }
    }

}
