## Test header (tentamen voorblad)

- **Teacher** Michiel Noback (NOMI), to be reached at +31 50 595 4691
- **Scanned by** KEMC
- **Test size** 6 assignments
- **Aiding materials** Computer on the BIN network
- **Supplementary materials**
    - The Java gitbook, on your Desktop

- **TO BE SUBMITTED  
  This project, as `zip` archive. Use this name for the zip:  
  `WebbasedAssessment<YOURNAME>-<YOURSTUDENTNUMBER>.zip`**

## Instructions
This is a testing assessment. Check out the Solutions branch to see what you were supposed to create.

Folder `src/main` is the base folder where you will find Java class and other files that may be referred to from 
within the assignments. Some _boilerplate_ code has already been provided in class `WebConfig`. 
Call `WebConfig.getDefaultTemplateEngine()` to get a link to the template engine.

You may create `main()` methods wherever you want. Alternatively, you can create test classes and -methods using JUnit.

<hr> 

## The Assignments

These assignments revolve around the creation of a simple sequence analysis service for Proteins.

<hr> 

### Assignment 1: A fragments.html page (10 points)

Create a Thymeleaf page that can serve as a container/template for several fragments to be reused in this application.
This `fragments.html` page should have this element:

1. A fragment named `footer` where you place a copyright notice in which (a) the year, (b) the name and (c) 
the email of the web admin can be dynamically inserted (i.e. parameters for the fragment). 
The email needs to be put in a real email link. The fragment will be processed into something like this:

```html
&copy; Copyright Henk (2019). Admin can be contacted by <a href="mailto:user@example.com?SUBJECT=Website">email</a>
```
And rendered like this:

&copy; Copyright Henk (2019). Admin can be contacted by <a href="mailto:user@example.com?SUBJECT=Website">email</a>

We'll use this fragment later.  

<hr> 
 
### Assignment 2: Modify `web.xml` deployment descriptor (3 points)

Add a set of variables to the provided deployment descriptor:  
- admin email
- admin name 

<hr> 

### Assignment 3: Create a servlet (8 points)

#### Assignment 3A: Create and configure (3 points)

Create a servlet called `Servlet` and make it 'listen' to get requests at URL `localhost:8080/do.analysis`. 
Make sure that the servlet is instantiated and initialized at application startup.

#### Assignment 3B: Initialize (3 points)

Read the `web.xml` variables (assignment 2) at servlet initialization and store them as instance variables. 

#### Assignment 3C: Handle requests (2 points)

Let `GET` requests be forwarded to the Thymeleaf template `protein_analysis_request_form.html` (created in the 
next assignment). That page should receive the correct parameters to be included within the footer fragment 
created in assignment 1. If you failed at assignment 2 or 3B you can simply pass the hardcoded values 
`2019`, `Piet` and `piet@example.com` to the Thymeleaf template. 

<hr>  

### Assignment 4: A Thymeleaf template (5 points)

Create a Thymeleaf page called `protein_analysis_request_form.html`.
Include the footer and make sure the values for year, admin-name and -email are included.

<hr>  

### Assignment 5: A sequence analysis request form (10 points)

Create a web form within page `protein_analysis_request_form.html` that can be used to request a protein sequence 
analysis.

This form should include these elements:
1. The first element should be a simple text field for the name of the sequence. The name may be between 
  3 and 20 characters long.
2. The second element should be the sequence itself, in a text box of sufficient size. The sequence may be 
  1 to 1000 characters long.
3. The third element is the submit button. After submitting, the form data should be sent to the `doPost()` 
  method of the servlet created in Assignment 3. 

<hr>  

### Assignment 6: Servlet Request handling (15 points)

Within `Servlet`, deal with the submitted form data.  
Look carefully at class `AminoAcid` before you proceed! You are not allowed to modify this class!
Class `TestSequences` provides some sequences for your convenience.

#### Assignment 6A: Check request data (7 points)

Verify the submitted sequence: if it is not a valid protein sequence, forward back to the request form and 
display an informative error message there in a conditionally displayed element.
Hint: don't forget to remove any trailing whitespace from the submitted sequence.

#### Assignment 6B: Analyse sequence (8 points)

Assuming the sequence was correct: Determine  
1. the molecular weight of the sequence 
2. the amino acid frequencies within the submitted sequence.
Pass this data to page `amino_acid_frequencies.html`.

If you do not succeed here you can simply pass some hardcoded data.

<hr>  

### Assignment 7: Display results (10 points)

If you have not done so already, create template `amino_acid_frequencies.html`.
Within this page, display the following information:

1. The name of the sequence
2. The molecular weight of the sequence 
3. The amino acid frequencies of the sequence in the form of a table with columns `single letter code` and `frequency`. The single-letter code should be a link pointing to a servlet, `/give.detail` (that you are not going to implement) serving details for the amino acids. Use request parameters for this. 

Besides this, the page should have a link pointing back to the `protein_analysis_request_form.html` to support analysing a new sequence.

<hr>  

### Assignment 8: An intercept (8 points)

Create a catch-all web filter that intercepts all http `GET` requests, whatever the requested resource 
(e.g. "/home", "/index.html"). Let all these requests redirect to the `/do.analysis` servlet URL.

<hr>

That's it.


